package sort

import "sort"

type sorter struct {
	less   func(int, int) bool
	swap   func(int, int)
	length int
}

func (s sorter) Len() int           { return s.length }
func (s sorter) Less(i, j int) bool { return s.less(i, j) }
func (s sorter) Swap(i, j int)      { s.swap(i, j) }
func Sort(less func(int, int) bool, swap func(int, int), length int) {
	sort.Sort(sorter{less: less, swap: swap, length: length})
}
func IsSorted(less func(int, int) bool, length int) bool {
	return sort.IsSorted(sorter{less: less, length: length})
}
